maxVal = 4294967296
minVal = 0

nextTry = (maxVal - minVal) / 2
print('Try : ', nextTry)

while(maxVal>minVal):
    nextInput = ''
    while(nextInput != 'h' and nextInput != 'l'):
        nextInput = input('Input h or l: ')
    
    if(nextInput == 'h'):
        maxVal = nextTry
    else:
        minVal = nextTry
    nextTry = ((maxVal - minVal) / 2) + minVal
    print('Try : ', int(nextTry))
    
    
