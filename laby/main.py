import socket
import sys, struct

sys.setrecursionlimit(2000)
BUFFER_SIZE = 2000
sock = socket.socket()
sock.connect(('ctf.cfiul.ca', 24001))

sock.recv(1000)

labyritnth = ""

for i in range(6): 
    receivedLine = sock.recv(BUFFER_SIZE).decode()
    labyritnth+= receivedLine

labyritnth = labyritnth.split('\n')[1:]

labyritnth = list(map(lambda labyLine: labyLine.replace(',', ''), labyritnth))
visited = [[False for i in range(len(labyritnth[0])) ] for y in range(len(labyritnth))]

print("\n".join(labyritnth))
def findEntrance():
    for (index, line) in enumerate(labyritnth):
        if(line[0] == "S"):
            return (index, 0)

entrance = findEntrance()

def isOutside(position):
    (x, y) = position
    return x < 0 or x >= len(labyritnth) or y < 0 or y >= len(labyritnth[0])

def testPosition(position) :
    if(isOutside(position)):
        return False
    (x, y) = position
    char = labyritnth[x][y]
    if(char != ' ' and char != 'E'):
        return False
    if(visited[x][y]):
        return False
    return True

def findPossibleDirections(position):
    directions = []
    (x, y) = position
    
    if(testPosition((x-1, y))):
        directions.append(((x-1, y), 'UP'))
    if(testPosition((x+1, y))):
        directions.append(((x+1, y), 'DOWN'))
    if(testPosition((x, y-1))):
        directions.append(((x, y-1), 'LEFT'))
    if(testPosition((x, y+1))):
        directions.append(((x, y+1), 'RIGHT'))

    return directions

def resolve(start, path=[]):
    (x, y) = start
    visited[x][y] = True

    directions = findPossibleDirections(start)
    for (targetPos, direction) in directions:
        (x, y) = targetPos
        if(not visited[x][y]):

            newPaths = path[:]
            newPaths.append(direction)
            
            if(labyritnth[x][y] == "E"):
                return newPaths

            result = resolve(targetPos, newPaths)
            if result is not None:
                return result
    
    

solution = resolve(entrance)
message = ','.join(solution)

sock.send(message.encode())
sock.send(message.encode())
sock.send(message.encode())

print(message)
print(sock.recv(BUFFER_SIZE).decode())