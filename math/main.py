from operator import mul
import functools

numbers = []
with open('data.txt') as file:
    numbers = list(map(lambda x: int(x.strip()), file.read().replace('\n', ' ').split()))

summOfFirsts = sum(numbers)
print(functools.reduce(mul,list(map(lambda x: int(x), list(str(summOfFirsts)))), 1))
